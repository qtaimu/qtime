package com.example.qtime.Utils;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

public class TypeFace {

    public static void ovverideFont(Context context, String defaultFontNameToOvveride, String customFontFileNameInAssets){

        try{
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOvveride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        }catch (Exception e){

        }

    }

}
