package com.example.qtime;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.qtime.Utils.Utils;

import es.dmoral.toasty.Toasty;

public class NoconnectionActivity extends AppCompatActivity {

    Button connect;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noconnection);

        connect = findViewById(R.id.btnNoConnected);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils utils = new Utils(getApplicationContext());
                if (! utils.isNetworkAvailable()){

                }else{
                    Intent intent = new Intent(NoconnectionActivity.this, HospitalFragment.class);
                    startActivity(intent);
                }
            }
        });
    }
}
