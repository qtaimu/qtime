package com.example.qtime.Model;

import java.util.List;

public class ResponseRegisterUserLama {

    String kode, pesan;
    List<RegisterUserLamaModel> result;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<RegisterUserLamaModel> getResult() {
        return result;
    }

    public void setResult(List<RegisterUserLamaModel> result) {
        this.result = result;
    }
}
