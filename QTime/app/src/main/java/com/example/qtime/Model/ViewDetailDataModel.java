package com.example.qtime.Model;

public class ViewDetailDataModel {

    private String KodePoliHospital;
    private String CodeHospital;
    private String HospitalName;
    private String Address;
    private String KodePos;
    private String NoHp;
    private String FotoHospital;
    private String FotoHospital2;
    private String FotoHospital3;
    private String IdPoli;
    private String NamaPoli;
    private String DeskPoli;
    private String IconPoli;

    public String getKodePoliHospital() {
        return KodePoliHospital;
    }

    public void setKodePoliHospital(String kodePoliHospital) {
        KodePoliHospital = kodePoliHospital;
    }

    public String getCodeHospital() {
        return CodeHospital;
    }

    public void setCodeHospital(String codeHospital) {
        CodeHospital = codeHospital;
    }

    public String getHospitalName() {
        return HospitalName;
    }

    public void setHospitalName(String hospitalName) {
        HospitalName = hospitalName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getKodePos() {
        return KodePos;
    }

    public void setKodePos(String kodePos) {
        KodePos = kodePos;
    }

    public String getNoHp() {
        return NoHp;
    }

    public void setNoHp(String noHp) {
        NoHp = noHp;
    }

    public String getFotoHospital() {
        return FotoHospital;
    }

    public void setFotoHospital(String fotoHospital) {
        FotoHospital = fotoHospital;
    }

    public String getFotoHospital2() {
        return FotoHospital2;
    }

    public void setFotoHospital2(String fotoHospital2) {
        FotoHospital2 = fotoHospital2;
    }

    public String getFotoHospital3() {
        return FotoHospital3;
    }

    public void setFotoHospital3(String fotoHospital3) {
        FotoHospital3 = fotoHospital3;
    }

    public String getIdPoli() {
        return IdPoli;
    }

    public void setIdPoli(String idPoli) {
        IdPoli = idPoli;
    }

    public String getNamaPoli() {
        return NamaPoli;
    }

    public void setNamaPoli(String namaPoli) {
        NamaPoli = namaPoli;
    }

    public String getDeskPoli() {
        return DeskPoli;
    }

    public void setDeskPoli(String deskPoli) {
        DeskPoli = deskPoli;
    }

    public String getIconPoli() {
        return IconPoli;
    }

    public void setIconPoli(String iconPoli) {
        IconPoli = iconPoli;
    }

}
