package com.example.qtime.Model;

import java.util.List;

public class ResponseModelData {

    String kode, pesan;
    List<ViewDetailDataModel> result;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<ViewDetailDataModel> getResult() {
        return result;
    }

    public void setResult(List<ViewDetailDataModel> result) {
        this.result = result;
    }
}
