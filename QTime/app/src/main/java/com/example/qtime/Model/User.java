package com.example.qtime.Model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class User {

    private String iduser;
    private String fullName;
    private String displayname;
    private String photo;
    private String email;
    private HashMap<String,Object> timestampJoined;

    public User(){}

    public User(String iduser, String fullName, String displayname, String photo, String email, HashMap<String, Object> timestampJoined) {
        this.iduser = iduser;
        this.fullName = fullName;
        this.displayname = displayname;
        this.photo = photo;
        this.email = email;
        this.timestampJoined = timestampJoined;
    }

    public String getIduser() {
        return iduser;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDisplayname() {
        return displayname;
    }

    public String getPhoto() {
        return photo;
    }

    public String getEmail() {
        return email;
    }

    public HashMap<String, Object> getTimestampJoined() {
        return timestampJoined;
    }
}
