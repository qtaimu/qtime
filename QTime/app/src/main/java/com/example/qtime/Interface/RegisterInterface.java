package com.example.qtime.Interface;

import com.example.qtime.Base.BaseResponse;
import com.example.qtime.Network.Config;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterInterface {

    @FormUrlEncoded
    @POST(Config.API_LOGIN)
    Call<BaseResponse> loginPatient(@Field("IdPatient") String IdPatient,
                                    @Field("PatientName") String PatientName,
                                    @Field("PatientEmail") String PatientEmail,
                                    @Field("Photo") String Photo,
                                    @Field("Phone") String Phone,
                                    @Field("Address") String Address,
                                    @Field("TglLahir") String TglLahir);

}
