package com.example.qtime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.qtime.Adapter.ProfileAdapter;
import com.example.qtime.Model.ProfileList;
import com.example.qtime.Utils.PrefManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private ArrayList<String> list = new ArrayList<String>();

    Context mContext = this;

    private Toolbar toolbar;
    ListView lvProfileUser;
    FloatingActionButton btnEdit;

    CircleImageView imageUser;
    TextView nameUser, emailUser;
    private String mUsername, mEmail;

    PrefManager prefManager;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();

        toolbar = findViewById(R.id.toolbar_profile);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //OptionList();

        lvProfileUser = findViewById(R.id.lvProfileUser);
        list.add("Linked Account");
        list.add("Language");
        list.add("Change Password");
        list.add("Logout");
        ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        lvProfileUser.setAdapter(mArrayAdapter);
        lvProfileUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (list.get(position).equals("Logout")){
                    AlertLogout();
                }
            }
        });

        btnEdit = findViewById(R.id.btnEditProfile);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent update = new Intent(ProfileActivity.this, UpdateProfileActivity.class);
                startActivity(update);
            }
        });

        imageUser = findViewById(R.id.profile_image);
        nameUser = findViewById(R.id.tvNameUser);
        emailUser = findViewById(R.id.tvEmailUser);

        prefManager = new PrefManager(this);
        mUsername = prefManager.getName();
        mEmail = prefManager.getUserEmail();
        String uri = prefManager.getPhoto();
        Uri mPhotoUri = Uri.parse(uri);

        nameUser.setText(mUsername);
        emailUser.setText(mEmail);

        Picasso.with(mContext)
                .load(mPhotoUri)
                .placeholder(android.R.drawable.sym_def_app_icon)
                .error(android.R.drawable.sym_def_app_icon)
                .into(imageUser);
        configureSignIn();
    }

    public void configureSignIn(){
    // Configure sign-in to request the user's basic profile like name and email
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();
        mGoogleApiClient.connect();
    }

    private void AlertLogout()
    {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Dialog);
        }
        builder.setTitle("Keluar QTime")
                .setMessage("Anda yakin ingin keluar ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void signOut(){
        new PrefManager(mContext).clear();
        mAuth.signOut();

        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Intent inten = new Intent(ProfileActivity.this, MainActivity.class);
                        startActivity(inten);
                    }
                }
        );
    }


    private void OptionList(){
        ArrayList<ProfileList> arrayOfOption = ProfileList.getProfile();
        ProfileAdapter adapter = new ProfileAdapter(this, arrayOfOption);
        ListView listView = findViewById(R.id.lvProfileUser);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
