package com.example.qtime.Interface;

import com.example.qtime.Base.BaseResponse;
import com.example.qtime.Model.ResponseModel;
import com.example.qtime.Model.ResponseModelData;
import com.example.qtime.Model.ResponseNewsModel;
import com.example.qtime.Model.ResponsePaymentModel;
import com.example.qtime.Model.ResponseRegisterUserLama;
import com.example.qtime.Network.Config;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Interface {

    @GET("read.php")
    Call<ResponseModel> getDataHospital();

    @FormUrlEncoded
    @POST("readViewData.php")
    Call<ResponseModelData> getDetailData(@Field("CodeHospital") String CodeHospital);

    @FormUrlEncoded
    @POST("readPaymentHospital.php")
    Call<ResponsePaymentModel> getPaymentData(@Field("CodeHospital") String CodeHospital);

    @GET("readNews.php")
    Call<ResponseNewsModel> getNewsData();

    @FormUrlEncoded
    @POST("registerUserLama.php")
    Call<ResponseRegisterUserLama> insertUserLama(@Field("IdUserLama") String IdUserLama,
                                                  @Field("CodeHospital") String CodeHospital,
                                                  @Field("IdPoli") String IdPoli,
                                                  @Field("IdPatient") String IdPatient,
                                                  @Field("NoRm") String NoRm,
                                                  @Field("TglDaftar") String TglDaftar,
                                                  @Field("Payment") String Payment);

}
