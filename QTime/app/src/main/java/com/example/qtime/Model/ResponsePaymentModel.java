package com.example.qtime.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePaymentModel {

    String kode, pesan;

    List<PaymentModel> result;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<PaymentModel> getResult() {
        return result;
    }

    public void setResult(List<PaymentModel> result) {
        this.result = result;
    }
}
