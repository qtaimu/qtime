package com.example.qtime.Network;

public class Config {

    public static final String BASE_URL = "http://192.168.100.42"; // Your Local IP Address

    public static final String API_URL = BASE_URL + "/QtimeApi";

    public static final String API_LOGIN = API_URL + "/login.php";
    public static final String API_REGISTER = API_URL + "/register.php";

}
