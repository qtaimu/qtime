package com.example.qtime;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qtime.Adapter.DetailPenyakitAdapter;
import com.example.qtime.Adapter.SlidingImageAdapter;
import com.example.qtime.Interface.Interface;
import com.example.qtime.Model.ImageModel;
import com.example.qtime.Model.ResponseModelData;
import com.example.qtime.Model.ViewDetailDataModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Utils.Utils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HospitalDetail extends AppCompatActivity {

    RelativeLayout rela2;
    TextView name, address, nohp, kode;
    //ImageView fotors;
    FloatingActionButton direct;
    Toolbar toolbar;
    Button btnRegister;
    Context ctx;

    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private RecyclerView.Adapter mAdapter;
    private List<ViewDetailDataModel> mItems = new ArrayList<>();

    private LinearLayout mLinearLayout;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<ImageModel> imageModelArrayList;

    public static final String base_url = "http://192.168.100.42/QtimeApi/";

    private int[] myImageList = new int[]{R.drawable.rs, R.drawable.rs2,
            R.drawable.rs3,R.drawable.rs4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_detail);

        init();
        loadData();

        toolbar = findViewById(R.id.toolbarHospitalDetail);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadDataPoliDetail();
        //setupSlider();

        Utils utils = new Utils(this);
        if (!utils.isNetworkAvailable()){
            final Snackbar snackbar = Snackbar.make(rela2, "Tidak ada koneksi internet", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }



    private void init() {
        name = findViewById(R.id.hospital_name);
        address = findViewById(R.id.hospital_address);
        nohp = findViewById(R.id.tvNoHp);
        kode = findViewById(R.id.tvHospitalKode);
        //fotors = findViewById(R.id.ivFotoDetailHospital);
        direct = findViewById(R.id.btn_direct_maps);
        btnRegister = findViewById(R.id.btRegisterPoli);
        mLinearLayout = findViewById(R.id.pagesContainer);

        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();

        mPager = findViewById(R.id.viewPager);
        mPager.setAdapter(new SlidingImageAdapter(HospitalDetail.this,imageModelArrayList));

        CirclePageIndicator indicator = findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        swipeRefreshLayout = findViewById(R.id.swipeDetail);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataPoliDetail();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        recyclerView = findViewById(R.id.recylerPenyakit);
        mManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mManager);
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL, 5));

        final float density = getResources().getDisplayMetrics().density;

    //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =imageModelArrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void loadDataPoliDetail()
    {
        Bundle getBundle = null;
        getBundle = this.getIntent().getExtras();
        Interface api = RetroServer.getClient(base_url).create(Interface.class);
        Call<ResponseModelData> getDataPoli = api.getDetailData(getBundle.getString("CodeHospital"));
        getDataPoli.enqueue(new Callback<ResponseModelData>() {
            @Override
            public void onResponse(Call<ResponseModelData> call, Response<ResponseModelData> response) {
                Log.d("RETRO", "RESPONSE : " + response.body().getKode());
                mItems = response.body().getResult();

                mAdapter = new DetailPenyakitAdapter(ctx, mItems);
                recyclerView.setAdapter(mAdapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseModelData> call, Throwable t) {

            }
        });
    }

    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 4; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }


    private void loadData()
    {
        final Intent data = getIntent();

        direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = "Jl. Raya Pajajaran No. 80";
                Intent maps = new Intent(getApplicationContext(), DirectionHospital.class);
                maps.putExtra("key", value);
                startActivity(maps);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goInput = new Intent(getApplicationContext(), PoliActivity.class);
                goInput.putExtra("CodeHospital", kode.getText().toString());
                goInput.putExtra("HospitalName", name.getText().toString());
                goInput.putExtra("HospitalAddress", address.getText().toString());
                startActivity(goInput);
            }
        });

        final String iddata = data.getStringExtra("CodeHospital");
        if (iddata != null){
            name.setText(data.getStringExtra("HospitalName"));
            address.setText(data.getStringExtra("Address"));
            nohp.setText(data.getStringExtra("NoHp"));
            kode.setText(data.getStringExtra("CodeHospital"));
            //Picasso.with(getApplicationContext()).load(base_url+data.getStringExtra("FotoHospital")).fit().centerCrop().into(fotors);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loadData();
    }


}
