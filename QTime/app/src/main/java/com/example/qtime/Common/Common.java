package com.example.qtime.Common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.example.qtime.Model.HospitalDetailModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Remote.IGeoCoordinates;


public class Common {

    public static HospitalDetailModel hospitalRequests;
    public static final String base_url = "https://maps.googleapis.com";
    public static final String Address = "Jl. Raya Pajajaran No. 80";

    public static IGeoCoordinates getGeoCodeServices()
    {
        return RetroServer.getClient(base_url).create(IGeoCoordinates.class);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight)
    {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth/(float)bitmap.getWidth();
        float scaleY = newHeight/(float)bitmap.getHeight();
        float pivotX=0, pivotY=0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

}
