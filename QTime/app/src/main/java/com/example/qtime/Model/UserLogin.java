package com.example.qtime.Model;

import com.example.qtime.Base.BaseResponse;

public class UserLogin extends BaseResponse {

    private UserData data;

    public UserLogin() {
    }


    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

}
