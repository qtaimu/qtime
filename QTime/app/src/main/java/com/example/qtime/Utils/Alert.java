package com.example.qtime.Utils;

import android.app.Activity;
import android.content.Intent;

import com.example.qtime.MainActivity;
import com.example.qtime.ProfileActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Alert {

    public static void SuccessAlert(final Activity from, final Class<?> to, boolean finish, String contentText)
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(from, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText("Informasi");
        pDialog.setContentText(contentText);
        pDialog.setCancelable(false);
        pDialog.setConfirmText("OK");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Intent intent = new Intent(from, to);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                from.startActivity(intent);
                from.finish();
            }
        });
        pDialog.show();
    }

    public static void ErrorAlert(final Activity from, String contentText)
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(from, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText("Informasi");
        pDialog.setContentText(contentText);
        pDialog.setCancelable(false);
        pDialog.setConfirmText("OK");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.hide();
            }
        });
        pDialog.show();
    }

    public static void ConfirmAlert(final Activity from, String titleText, String contentText)
    {
        new SweetAlertDialog(from, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titleText)
                .setContentText(contentText)
                .setConfirmText("Ya !")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                    }
                })
                .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

}
