package com.example.qtime;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import com.example.qtime.Adapter.HospitalAdapter;
import com.example.qtime.Interface.Interface;
import com.example.qtime.Model.HospitalModel;
import com.example.qtime.Model.ResponseModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Utils.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HospitalFragment extends Fragment {

    RelativeLayout rela;
    private RecyclerView mRecyler;
    private RecyclerView.Adapter mAdapter;
    LinearLayoutManager manager;
    private RecyclerView.LayoutManager mManager;
    private List<HospitalModel> mItems = new ArrayList<>();
    Context context;

    private ShimmerFrameLayout mShimmerViewContainer;
    MaterialSpinner spinnerRs;
    SwipeRefreshLayout swipeRefreshLayout;
    public static final String base_url = "http://192.168.43.71/QtimeApi/";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_hospital_fragment, container, false);

        mRecyler = view.findViewById(R.id.recylerHospital);
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyler.setLayoutManager(mManager);

        rela = view.findViewById(R.id.rela);
        spinnerRs = view.findViewById(R.id.spinner_rs);
        spinnerRs.setItems("Nearby Location", "Short Queue", "Frequented Hospital");
        spinnerRs.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_SHORT).show();
            }
        });

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        loadDataHospital();

        swipeRefreshLayout = view.findViewById(R.id.myswipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataHospital();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    private void loadDataHospital()
    {
        Interface api = RetroServer.getClient(base_url).create(Interface.class);
        Call<ResponseModel> getdata = api.getDataHospital();
        getdata.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                Log.d("RETRO", "RESPONSE : " + response.body().getKode());
                mItems = response.body().getResult();

                mAdapter = new HospitalAdapter(getActivity(), mItems);
                mRecyler.setAdapter(mAdapter);
                mRecyler.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 5));
                mRecyler.setItemAnimator(new DefaultItemAnimator());
                mAdapter.notifyDataSetChanged();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d("RETRO", "FAILED : response gagal");
                final Utils utils = new Utils(getActivity());
                if (! utils.isNetworkAvailable()){
                    final Snackbar snackbar = Snackbar.make(rela, "Tidak ada koneksi internet", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }




}
