package com.example.qtime.Interface;

import com.example.qtime.Model.UserLogin;
import com.example.qtime.Network.Config;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginInterface {

    @FormUrlEncoded
    @POST(Config.API_LOGIN)
    Call<UserLogin> login(
            @Field("Email") String Email,
            @Field("Password") String Password);

}
