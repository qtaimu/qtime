package com.example.qtime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.qtime.HospitalDetail;
import com.example.qtime.Model.HospitalModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.HolderData> {

    private List<HospitalModel> mList;
    private Context ctx;
    private static String base_url = "http://192.168.100.42/Qtime/view_super/img/";
    private int lastPosition=-1;
    private View mView;

    public HospitalAdapter(Context ctx, List<HospitalModel> mList) {
        this.ctx = ctx;
        this.mList = mList;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_list_view, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        HospitalModel dm = mList.get(position);
        holder.nama.setText(dm.getHospitalName());
        holder.lokasi.setText(dm.getAddress());
        Picasso.with(ctx).load(base_url + mList.get(position).getFotoHospital()).fit().centerCrop().into(holder.foto_rs);
        setAnimation(mView, position);
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class HolderData extends RecyclerView.ViewHolder{

        TextView nama, lokasi;
        ImageView foto_rs;
        HospitalModel dm;

        public HolderData(View v) {
            super(v);
            mView = v;

            nama = v.findViewById(R.id.tvHospitalName);
            lokasi = v.findViewById(R.id.tvHospitalAddress);
            foto_rs = v.findViewById(R.id.ivHospital);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, HospitalDetail.class);
                    goInput.putExtra("CodeHospital", dm.getCodeHospital());
                    goInput.putExtra("HospitalName", dm.getHospitalName());
                    goInput.putExtra("Jenis", dm.getJenis());
                    goInput.putExtra("Kelas", dm.getKelas());
                    goInput.putExtra("Direktur", dm.getDirektur());
                    goInput.putExtra("Pemilik", dm.getPemilik());
                    goInput.putExtra("Address", dm.getAddress());
                    goInput.putExtra("KabKot", dm.getKabKot());
                    goInput.putExtra("KodePos", dm.getKodePos());
                    goInput.putExtra("NoHp", dm.getNoHp());
                    goInput.putExtra("Fax", dm.getFax());
                    goInput.putExtra("Email", dm.getEmail());
                    goInput.putExtra("TglUpdate", dm.getTglUpdate());
                    goInput.putExtra("TglRegistrasi", dm.getTglRegistrasi());
                    goInput.putExtra("FotoHospital", dm.getFotoHospital());
                    ctx.startActivity(goInput);
                }
            });
        }

    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition) {
            lastPosition = position;
            Animation animation = AnimationUtils.loadAnimation(mView.getContext(), R.anim.item_animation_from_bottom);
            viewToAnimate.startAnimation(animation);
        }
    }

}
