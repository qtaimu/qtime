package com.example.qtime.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qtime.Model.ViewDetailDataModel;
import com.example.qtime.R;
import com.example.qtime.RegisterUserLama;
import com.squareup.picasso.Picasso;

import java.util.List;

import es.dmoral.toasty.Toasty;


public class ViewDataAdapter extends RecyclerView.Adapter<ViewDataAdapter.HolderData> {

    private List<ViewDetailDataModel> modelList;
    private Context ctx;
    private static String base_url = "http://192.168.100.42/Qtime/view_super/img/";
    private int lastPosition=-1;
    private View mView;
    Dialog myDialog;
    Activity mActivity = null;

    public ViewDataAdapter(Context ctx, List<ViewDetailDataModel> modelList){
        this.ctx = ctx;
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_poli_list_view, parent, false);
        final HolderData holder = new HolderData(layout);

        holder.item_poli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new BottomSheetDialog(parent.getContext());
                d.setContentView(R.layout.custom_dialog_poli);
                TextView nama = d.findViewById(R.id.tvPoliDialog);
                LinearLayout lama, baru;
                lama = d.findViewById(R.id.lilaLama);
                baru = d.findViewById(R.id.lilaBaru);

                lama.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent goInput = new Intent(ctx, RegisterUserLama.class);
                        goInput.putExtra("CodeHospital", holder.dm.getCodeHospital());
                        goInput.putExtra("IdPoli", holder.dm.getIdPoli());
                        goInput.putExtra("NamaPoli", holder.dm.getNamaPoli());
                        goInput.putExtra("DeskripsiPoli", holder.dm.getDeskPoli());
                        goInput.putExtra("IconPoli", holder.dm.getIconPoli());
                        ctx.startActivity(goInput);
                        d.dismiss();
                    }
                });

                baru.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toasty.success(ctx,"Register Baru", Toast.LENGTH_SHORT).show();
                        d.dismiss();
                    }
                });

                nama.setText(" ( Poli : " + modelList.get(holder.getAdapterPosition()).getNamaPoli() + " ) ");
                d.setCancelable(true);
                d.show();
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        ViewDetailDataModel dm = modelList.get(position);
        holder.nama.setText(dm.getNamaPoli());
        Picasso.with(ctx).load(base_url + modelList.get(position).getIconPoli()).fit().centerCrop().into(holder.icon);
        setAnimation(mView, position);
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    class HolderData extends RecyclerView.ViewHolder{

        private LinearLayout item_poli;
        TextView nama;
        ImageView icon;
        ViewDetailDataModel dm;

        public HolderData(View itemView) {
            super(itemView);
            mView = itemView;
            item_poli = itemView.findViewById(R.id.poli_item);
            nama = itemView.findViewById(R.id.tvNamePoli);
            icon = itemView.findViewById(R.id.ivLogoPoli);

            item_poli.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition) {
            lastPosition = position;
            Animation animation = AnimationUtils.loadAnimation(mView.getContext(), R.anim.slide_left_to_right);
            viewToAnimate.startAnimation(animation);
        }
    }

}
