package com.example.qtime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qtime.DirectionHospital;
import com.example.qtime.Model.HospitalDetailModel;
import com.example.qtime.PoliActivity;
import com.example.qtime.R;

import java.lang.ref.WeakReference;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class HospitalDetailAdapter extends RecyclerView.Adapter<HospitalDetailAdapter.HolderData> {

    private List<HospitalDetailModel> mList;
    private Context ctx;
    private View mView;

    public HospitalDetailAdapter(List<HospitalDetailModel> mList, Context ctx) {
        this.mList = mList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_hospital_detail, parent, false);
        return new HolderData(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        holder.bindItem(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class HolderData extends RecyclerView.ViewHolder {

        TextView name, address;
        ImageView  foto_rs;
        private View view;

        Button register;
        FloatingActionButton direct;
        HospitalDetailModel dm;

        public HolderData(View view) {
            super(view);
            mView = view;
            name = view.findViewById(R.id.hospital_name);
            address = view.findViewById(R.id.hospital_address);
            direct = view.findViewById(R.id.btn_direct_maps);
            register = view.findViewById(R.id.btRegisterPoli);
            this.view = view;
        }

        public void bindItem(final HospitalDetailModel detailModel){
            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(ctx, "halo", Toast.LENGTH_SHORT).show();
                    Log.d("HALO", "DFD");
                }
            });
        }

    }

}
