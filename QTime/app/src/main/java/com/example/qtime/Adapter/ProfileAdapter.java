package com.example.qtime.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.qtime.Model.ProfileList;
import com.example.qtime.R;

import java.util.ArrayList;

public class ProfileAdapter extends ArrayAdapter<ProfileList> {

    public ProfileAdapter(Context context, ArrayList<ProfileList> profileLists) {
        super(context, 0, profileLists);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.profile_list_view, parent, false);
        }

        ProfileList profileList = getItem(position);

        TextView tvOption = convertView.findViewById(R.id.tvOption);

        tvOption.setText(profileList.getOption());

        return convertView;
    }
}
