package com.example.qtime;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.qtime.Base.BaseResponse;
import com.example.qtime.Helper.DatePicker;
import com.example.qtime.Services.RegisterServices;
import com.example.qtime.Utils.PrefManager;
import com.rengwuxian.materialedittext.MaterialEditText;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreLoginActivity extends AppCompatActivity {

    MaterialEditText phone, address;
    Button save;
    Toolbar toolbar;

    private RegisterServices registerServices;
    private String mId, mName, mEmail, mPhoto, mPhone, mAddress, mBirth;
    PrefManager prefManager;
    DatePicker tgllahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_login);

        init();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(phone.getText()) || TextUtils.isEmpty(address.getText().toString()))
                {
                    Toasty.error(MoreLoginActivity.this, "Lengkapi Field yang kosong", Toast.LENGTH_SHORT).show();
                }else{
                    registerPatient();
                }
            }
        });
    }

    private void init()
    {
        phone = findViewById(R.id.phone_field);
        address = findViewById(R.id.address_field);
        tgllahir = findViewById(R.id.DpTanggalLahir);
        save = findViewById(R.id.btnSavePatient);

        toolbar = findViewById(R.id.toolbarLogin);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    void registerPatient()
    {
        prefManager = new PrefManager(this);
        mId = "PAT-"+prefManager.getUserId();
        mName = prefManager.getName();
        mEmail = prefManager.getUserEmail();
        mPhoto = prefManager.getPhotoUrl();
        mPhone = phone.getText().toString();
        mAddress = address.getText().toString();
        mBirth = tgllahir.getText().toString();
        registerServices = new RegisterServices(this);
        registerServices.doRegister(mId, mName, mEmail, mPhoto, mPhone, mAddress, mBirth, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                BaseResponse baseResponse = (BaseResponse) response.body();

                if (baseResponse != null)
                {
                    if (!baseResponse.isError()){
                        prefManager.saveIsLoggedIn(getApplicationContext(), true);
                        prefManager.savePhoneNumber(getApplicationContext(), mPhone);
                        prefManager.saveDateofBirth(getApplicationContext(), mBirth);
                        HomepageActivity.start(MoreLoginActivity.this);
                        MoreLoginActivity.this.finish();
                    }
                    Toasty.success(MoreLoginActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toasty.error(MoreLoginActivity.this, "An error occured !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
