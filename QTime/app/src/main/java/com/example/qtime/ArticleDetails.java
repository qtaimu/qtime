package com.example.qtime;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class ArticleDetails extends AppCompatActivity {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final long TIMESTAMP = 1000000000000L;

    Toolbar toolbar;
    ImageView fotoArticle;
    TextView title, author, content, timeago;
    private static String base_url = "http://192.168.100.42/Qtime/view_super/img/";
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);

        toolbar = findViewById(R.id.toolbarArticleDetail);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }


        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    private void init()
    {
        fotoArticle = findViewById(R.id.ivPictNews);
        title = findViewById(R.id.tvTitleNews);
        author = findViewById(R.id.tvNameAauthor);
        content = findViewById(R.id.tvContentNews);
        timeago = findViewById(R.id.tvTimeAgo);
        swipeRefreshLayout = findViewById(R.id.swipeAticleDetails);
        String ago = getTimeAgo(TIMESTAMP);
        timeago.setText(ago);
    }

    private void loadData()
    {
        final Intent data = getIntent();
        final String idnews = data.getStringExtra("kdNews");
        if (idnews != null)
        {
            Picasso.with(getApplicationContext()).load(base_url+data.getStringExtra("pictNews")).fit().centerCrop().into(fotoArticle);
            title.setText(data.getStringExtra("titleNews"));
            author.setText(data.getStringExtra("authorNews"));
            content.setText(data.getStringExtra("contentNews"));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
