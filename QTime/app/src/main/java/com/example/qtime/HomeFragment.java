package com.example.qtime;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qtime.Adapter.DailyInformationAdapter;
import com.example.qtime.Interface.Interface;
import com.example.qtime.Model.NewsModel;
import com.example.qtime.Model.ResponseNewsModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Utils.PrefManager;
import com.example.qtime.Utils.Utils;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    RelativeLayout relanews;
    RecyclerView mRecyler;
    private RecyclerView.LayoutManager mManager;
    private RecyclerView.Adapter mAdapter;
    private List<NewsModel> mItems = new ArrayList<>();
    Context context;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView name;

    PrefManager prefManager;
    private FirebaseAuth mAuth;
    private String mNameUser;

    public static final String base_url = "http://192.168.100.42/QtimeApi/";

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_home_fragment, container, false);

        relanews = view.findViewById(R.id.relaNews);
        mRecyler = view.findViewById(R.id.recylerDailyInformation);
        mManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyler.setLayoutManager(mManager);
        //mRecyler.setHasFixedSize(true);
        mRecyler.setNestedScrollingEnabled(false);
        mRecyler.setItemAnimator(new DefaultItemAnimator());
        swipeRefreshLayout = view.findViewById(R.id.homeSwipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataNews();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        loadDataNews();

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        prefManager = new PrefManager(getActivity());
        name = view.findViewById(R.id.tvUserNameHome);
        mNameUser = prefManager.getName();
        name.setText(mNameUser);

        return view;
    }

    public void loadDataNews()
    {
        Interface api = RetroServer.getClient(base_url).create(Interface.class);
        Call<ResponseNewsModel> getNews = api.getNewsData();
        getNews.enqueue(new Callback<ResponseNewsModel>() {
            @Override
            public void onResponse(Call<ResponseNewsModel> call, Response<ResponseNewsModel> response) {
                Log.d("RETRO", "RESPONSE : " + response.body().getKode());
                mItems = response.body().getResult();

                mAdapter = new DailyInformationAdapter(getActivity(), mItems);
                mRecyler.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<ResponseNewsModel> call, Throwable t) {
                Log.d("RETRO", "FAILED : response gagal");
                final Utils utils = new Utils(getActivity());
                if (! utils.isNetworkAvailable()){
                    final Snackbar snackbar = Snackbar.make(relanews, "Tidak ada koneksi internet", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

}
