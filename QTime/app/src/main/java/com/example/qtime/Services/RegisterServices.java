package com.example.qtime.Services;

import android.content.Context;

import com.example.qtime.Interface.Interface;
import com.example.qtime.Interface.RegisterInterface;
import com.example.qtime.Network.RetroBuilder;

import retrofit2.Callback;

public class RegisterServices {

    private RegisterInterface registerInterface;

    public RegisterServices(Context context) {
        registerInterface = RetroBuilder.bulder(context)
                .create(RegisterInterface.class);
    }

    public void doRegister(String id, String name, String email, String photo,
                           String phone, String address, String tgllahir, Callback callback) {
        registerInterface.loginPatient(id, name, email, photo, phone, address, tgllahir).enqueue(callback);
    }

}
