package com.example.qtime.Model;

public class PaymentModel {

    private String KodePayment;
    private String CodeHospital;
    private String HospitalName;
    private String Address;
    private String NoHp;
    private String Email;
    private String FotoHospital;
    private String IdPayment;
    private String payment;

    public String getKodePayment() {
        return KodePayment;
    }

    public void setKodePayment(String kodePayment) {
        KodePayment = kodePayment;
    }

    public String getCodeHospital() {
        return CodeHospital;
    }

    public void setCodeHospital(String codeHospital) {
        CodeHospital = codeHospital;
    }

    public String getHospitalName() {
        return HospitalName;
    }

    public void setHospitalName(String hospitalName) {
        HospitalName = hospitalName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getNoHp() {
        return NoHp;
    }

    public void setNoHp(String noHp) {
        NoHp = noHp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFotoHospital() {
        return FotoHospital;
    }

    public void setFotoHospital(String fotoHospital) {
        FotoHospital = fotoHospital;
    }

    public String getIdPayment() {
        return IdPayment;
    }

    public void setIdPayment(String idPayment) {
        IdPayment = idPayment;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
