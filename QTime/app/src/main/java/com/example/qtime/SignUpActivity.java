package com.example.qtime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.qtime.Adapter.StepperAdapter;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;

public class SignUpActivity extends AppCompatActivity {

    public static StepperLayout stepperLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        String[] titles = new String[]{"Name","Password","Region","Email","Agree"};
        Step[] steps = new Step[]{
                new SignUpBiodataFragment(),
                new SignUpPasswordFragment(),
                new SignUpRegionFragment(),
                new SignUpEmailFragment(),
                new SignUpAgreeFragment()
        };

        StepperAdapter stepperAdapter = new StepperAdapter(getSupportFragmentManager(), this, titles,steps);

        stepperLayout = findViewById(R.id.stepperLayout);
        stepperLayout.setAdapter(stepperAdapter);
    }
}
