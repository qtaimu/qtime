package com.example.qtime.Model;

public class NewsModel {

    private String kdNews;
    private String titleNews;
    private String contentNews;
    private String authorNews;
    private String pictNews;
    private String publishedDate;

    public String getKdNews() {
        return kdNews;
    }

    public void setKdNews(String kdNews) {
        this.kdNews = kdNews;
    }

    public String getTitleNews() {
        return titleNews;
    }

    public void setTitleNews(String titleNews) {
        this.titleNews = titleNews;
    }

    public String getContentNews() {
        return contentNews;
    }

    public void setContentNews(String contentNews) {
        this.contentNews = contentNews;
    }

    public String getAuthorNews() {
        return authorNews;
    }

    public void setAuthorNews(String authorNews) {
        this.authorNews = authorNews;
    }

    public String getPictNews() {
        return pictNews;
    }

    public void setPictNews(String pictNews) {
        this.pictNews = pictNews;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }
}
