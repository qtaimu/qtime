package com.example.qtime.Services;

import android.content.Context;

import com.example.qtime.Interface.LoginInterface;
import com.example.qtime.Network.RetroBuilder;

import retrofit2.Callback;

public class LoginServices {

    private LoginInterface loginInterface;

    public LoginServices(Context context){
        loginInterface = RetroBuilder.bulder(context)
                .create(LoginInterface.class);
    }

    public void doLogin(String Email, String Password, Callback callback){
        loginInterface.login(Email, Password).enqueue(callback);
    }

}
