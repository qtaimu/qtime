package com.example.qtime.Model;

public class HospitalModel {

    private String CodeHospital;
    private String HospitalName;
    private String Jenis;
    private String Kelas;
    private String Direktur;
    private String Pemilik;
    private String Address;
    private String KabKot;
    private String KodePos;
    private String NoHp;
    private String Fax;
    private String Email;
    private String TglUpdate;
    private String TglRegistrasi;

    private String FotoHospital;

    private Boolean pesan;

    public String getCodeHospital() {
        return CodeHospital;
    }

    public void setCodeHospital(String codeHospital) {
        CodeHospital = codeHospital;
    }

    public String getHospitalName() {
        return HospitalName;
    }

    public void setHospitalName(String hospitalName) {
        HospitalName = hospitalName;
    }

    public String getJenis() {
        return Jenis;
    }

    public void setJenis(String jenis) {
        Jenis = jenis;
    }

    public String getKelas() {
        return Kelas;
    }

    public void setKelas(String kelas) {
        Kelas = kelas;
    }

    public String getDirektur() {
        return Direktur;
    }

    public void setDirektur(String direktur) {
        Direktur = direktur;
    }

    public String getPemilik() {
        return Pemilik;
    }

    public void setPemilik(String pemilik) {
        Pemilik = pemilik;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getKabKot() {
        return KabKot;
    }

    public void setKabKot(String kabKot) {
        KabKot = kabKot;
    }

    public String getKodePos() {
        return KodePos;
    }

    public void setKodePos(String kodePos) {
        KodePos = kodePos;
    }

    public String getNoHp() {
        return NoHp;
    }

    public void setNoHp(String noHp) {
        NoHp = noHp;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTglUpdate() {
        return TglUpdate;
    }

    public void setTglUpdate(String tglUpdate) {
        TglUpdate = tglUpdate;
    }

    public String getTglRegistrasi() {
        return TglRegistrasi;
    }

    public void setTglRegistrasi(String tglRegistrasi) {
        TglRegistrasi = tglRegistrasi;
    }

    public String getFotoHospital() {
        return FotoHospital;
    }

    public void setFotoHospital(String fotoHospital) {
        FotoHospital = fotoHospital;
    }

    public Boolean getPesan() {
        return pesan;
    }

    public void setPesan(Boolean pesan) {
        this.pesan = pesan;
    }
}
