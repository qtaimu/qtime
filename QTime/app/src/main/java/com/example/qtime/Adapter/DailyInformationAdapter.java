package com.example.qtime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.example.qtime.ArticleDetails;
import com.example.qtime.Model.NewsModel;
import com.example.qtime.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DailyInformationAdapter extends RecyclerView.Adapter<DailyInformationAdapter.HolderData> {

    public static final String ACTION_LIKE_BUTTON_CLICKED = "action_like_button_button";
    private List<NewsModel> mList;
    private Context ctx;
    private View mView;
    int counter;
    private static String base_url = "http://192.168.100.42/Qtime/view_super/img/";

    public DailyInformationAdapter( Context ctx, List<NewsModel> mList) {
        this.ctx = ctx;
        this.mList = mList;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_information_listview, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        NewsModel di = mList.get(position);
        holder.dailyName.setText(di.getTitleNews());
        holder.dailyDescription.setText(di.getContentNews());
        Picasso.with(ctx).load(base_url + mList.get(position).getPictNews()).fit().centerCrop().into(holder.dailyImage);
        holder.dailyLikeImage.setTag(R.drawable.ic_md_thumbs_up);
        holder.nm = di;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends RecyclerView.ViewHolder{

        NewsModel nm;
        ImageView dailyImage, dailyLikeImage, dailyCommentImage, dailyPlaneImage;
        TextView dailyName, dailyDescription, dailyLike;
        TextSwitcher tsLikeCounter;

        public HolderData(View v) {
            super(v);

            counter = 120;
            dailyImage = v.findViewById(R.id.ivImageDaily);
            dailyName = v.findViewById(R.id.tvNameDaily);
            dailyDescription = v.findViewById(R.id.tvDescriptionDaily);
            dailyLikeImage = v.findViewById(R.id.ivLike);
            dailyLike = v.findViewById(R.id.tvLikeDaily);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, ArticleDetails.class);
                    goInput.putExtra("kdNews", nm.getKdNews());
                    goInput.putExtra("titleNews", nm.getTitleNews());
                    goInput.putExtra("contentNews", nm.getContentNews());
                    goInput.putExtra("authorNews", nm.getAuthorNews());
                    goInput.putExtra("pictNews", nm.getPictNews());
                    goInput.putExtra("publishedDate", nm.getPublishedDate());
                    ctx.startActivity(goInput);
                }
            });

            dailyLikeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = (int)dailyLikeImage.getTag();
                    if (id == R.drawable.ic_md_thumbs_up){
                        dailyLikeImage.setTag(R.drawable.ic_md_thumbs_up2);
                        dailyLikeImage.setImageResource(R.drawable.ic_md_thumbs_up2);
                        counter = counter + 1;
                        dailyLike.setText(Integer.toString(counter));
                    }else{
                        dailyLikeImage.setTag(R.drawable.ic_md_thumbs_up);
                        dailyLikeImage.setImageResource(R.drawable.ic_md_thumbs_up);
                        counter = counter - 1;
                        dailyLike.setText(Integer.toString(counter));
                    }
                }
            });
        }
    }

}
