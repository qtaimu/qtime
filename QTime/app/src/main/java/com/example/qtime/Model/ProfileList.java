package com.example.qtime.Model;

import java.util.ArrayList;

public class ProfileList {

    private String option;

    public ProfileList(String option) {
        this.option = option;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public static ArrayList<ProfileList> getProfile()
    {
        ArrayList<ProfileList> profile = new ArrayList<ProfileList>();
        profile.add(new ProfileList("Linked Account"));
        profile.add(new ProfileList("Language"));
        profile.add(new ProfileList("Change Password"));
        profile.add(new ProfileList("Logout"));
        profile.add(new ProfileList(""));
        return profile;
    }
}
