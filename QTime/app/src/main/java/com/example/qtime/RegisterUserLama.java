package com.example.qtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qtime.Helper.DatePicker;
import com.example.qtime.Interface.Interface;
import com.example.qtime.Model.PaymentModel;
import com.example.qtime.Model.ResponseModelData;
import com.example.qtime.Model.ResponsePaymentModel;
import com.example.qtime.Model.ViewDetailDataModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Utils.PrefManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserLama extends AppCompatActivity {

    Toolbar toolbar;

    TextView namaPoli, deskripsiPoli;
    ImageView iconPoli;
    private static final String base_url = "http://192.168.100.42/Qtime/view_super/img/";
    public static final String base_url2 = "http://192.168.100.42/QtimeApi/";


    PrefManager prefManager;
    private FirebaseAuth mAuth;
    private String mIdUser, mGiveName, mFullName, mPhone, mBirth;
    EditText iduser, namalengkap, phone, dateforregisration, birth;
    Spinner payment;

    private List<PaymentModel> mItems = new ArrayList<>();
    Button registerUserLamaa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user_lama);

        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        prefManager = new PrefManager(this);

        init();
        loadData();
        initSpinnerPayment();

        toolbar = findViewById(R.id.toolbarRegisterUserLama);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init()
    {
        namaPoli = findViewById(R.id.tvRegisterLamaNamaPoli);
        deskripsiPoli = findViewById(R.id.tvRegisterLamaDeskripsiPoli);
        iconPoli = findViewById(R.id.ivRegisterLamaIconPoli);

        iduser = findViewById(R.id.tvRegisterLamaIdUser);
        namalengkap = findViewById(R.id.tvRegisterLamaNamaUser);
        dateforregisration = findViewById(R.id.etDateRegistration);
        phone = findViewById(R.id.tvRegisterLamaNoHp);
        birth = findViewById(R.id.et_birth);
        payment = findViewById(R.id.spinnnerPayment);
        registerUserLamaa = findViewById(R.id.btnRegisterUserLama);

        mIdUser = "PAT-"+prefManager.getUserId();
        mFullName = prefManager.getName();
        mGiveName = prefManager.getGivenName();
        mPhone = prefManager.getPhoneNumber();
        mBirth = prefManager.getDateOfBirth();
        iduser.setText(mIdUser);
        namalengkap.setText(mFullName);
        phone.setText(mPhone);
        birth.setText(mBirth);

        Calendar cal = Calendar.getInstance();
        //hh:mm:ss
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date_str = df.format(cal.getTime());
        dateforregisration.setText(date_str);
    }

    private void loadData()
    {
        final Intent data = getIntent();
        final String iddata = data.getStringExtra("IdPoli");

        String nama = data.getStringExtra("NamaPoli");
        namaPoli.setText(nama);
        deskripsiPoli.setText(data.getStringExtra("DeskripsiPoli"));
        Picasso.with(getApplicationContext()).load(base_url+data.getStringExtra("IconPoli")).fit().centerCrop().into(iconPoli);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initSpinnerPayment()
    {
        Bundle getBundle = null;
        getBundle = this.getIntent().getExtras();
        Interface api = RetroServer.getClient(base_url2).create(Interface.class);
        Call<ResponsePaymentModel> getViewData = api.getPaymentData(getBundle.getString("CodeHospital"));
        getViewData.enqueue(new Callback<ResponsePaymentModel>() {
            @Override
            public void onResponse(Call<ResponsePaymentModel> call, Response<ResponsePaymentModel> response) {
                if (response.isSuccessful())
                {
                    mItems = response.body().getResult();
                    List<String> listSpinner = new ArrayList<String>();
                    for (int i = 0; i < mItems.size(); i++){
                        listSpinner.add(mItems.get(i).getPayment());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, listSpinner);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    payment.setAdapter(adapter);
                }else{
                    Toasty.error(getApplicationContext(), "Gagal Mengambil data dosen", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponsePaymentModel> call, Throwable t) {
                Toasty.error(getApplicationContext(), "Opps, No Internect Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
