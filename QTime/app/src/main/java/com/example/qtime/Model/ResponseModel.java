package com.example.qtime.Model;

import java.util.List;

public class ResponseModel {

    String kode, pesan;
    List<HospitalModel> result;

    public String getKode() {   
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<HospitalModel> getResult() {
        return result;
    }

    public void setResult(List<HospitalModel> result) {
        this.result = result;
    }

}
