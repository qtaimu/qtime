package com.example.qtime;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.qtime.Utils.PrefManager;

public class SplashActivity extends AppCompatActivity {

    int time = 3000;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler();
    }

    private void handler()
    {
        if(Build.VERSION.SDK_INT >= 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                PrefManager prefManager = new PrefManager(getApplicationContext());
                Intent intent;
                if (prefManager.getISLogged_IN())
                {
                    intent = new Intent(getApplicationContext(), HomepageActivity.class);
                }else{
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }
                startActivity(intent);
            }
        }, time);
    }

}
