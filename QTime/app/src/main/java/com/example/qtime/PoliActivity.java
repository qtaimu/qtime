package com.example.qtime;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qtime.Adapter.ViewDataAdapter;
import com.example.qtime.Interface.Interface;
import com.example.qtime.Model.ResponseModelData;
import com.example.qtime.Model.ViewDetailDataModel;
import com.example.qtime.Network.RetroServer;
import com.example.qtime.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PoliActivity extends AppCompatActivity {

    private RecyclerView mRecyler;
    private RecyclerView.Adapter mAdapter;
    private List<ViewDetailDataModel> mItems = new ArrayList<>();
    public static final String base_url = "http://192.168.100.42/QtimeApi/";

    TextView name;
    Toolbar toolbar;
    ProgressDialog pd;
    Context context;
    RelativeLayout rela2;
    SwipeRefreshLayout swipeRefreshLayout;
    Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poli);
        mRecyler = findViewById(R.id.recylerPoli);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        mRecyler.setLayoutManager(manager);
        rela2 = findViewById(R.id.rela2);
        swipeRefreshLayout = findViewById(R.id.swipePoli);

        toolbar = findViewById(R.id.toolbarPoli);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataPoli();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        name = findViewById(R.id.tvRsName);

        if(getIntent().getExtras()!=null){
            bundle = getIntent().getExtras();
            name.setText(bundle.getString("HospitalName"));
        }else{
            name.setText(getIntent().getStringExtra("HospitalName"));
        }

        loadDataPoli();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadDataPoli()
    {
        Bundle getBundle = null;
        getBundle = this.getIntent().getExtras();
        Interface api = RetroServer.getClient(base_url).create(Interface.class);
        Call<ResponseModelData> getViewData = api.getDetailData(getBundle.getString("CodeHospital"));
        getViewData.enqueue(new Callback<ResponseModelData>() {
            @Override
            public void onResponse(Call<ResponseModelData> call, Response<ResponseModelData> response) {
                Log.d("RETRO", "RESPONSE : " + response.body().getKode());
                mItems = response.body().getResult();

                mAdapter = new ViewDataAdapter(getApplicationContext(), mItems);
                mRecyler.setAdapter(mAdapter);
                mRecyler.setItemAnimator(new DefaultItemAnimator());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseModelData> call, Throwable t) {
                Log.d("RETRO", "FAILED : response gagal");
                final Utils utils = new Utils(getApplicationContext());
                if (! utils.isNetworkAvailable()){
                    final Snackbar snackbar = Snackbar.make(rela2, "Tidak ada koneksi internet", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }
}
