package com.example.qtime.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.qtime.Model.ViewDetailDataModel;
import com.example.qtime.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailPenyakitAdapter extends RecyclerView.Adapter<DetailPenyakitAdapter.HolderData> {

    private List<ViewDetailDataModel> mList;
    private Context ctx;
    private static String base_url = "http://192.168.100.42/Qtime/view_super/img/";

    public DetailPenyakitAdapter(Context ctx, List<ViewDetailDataModel> mList) {
        this.ctx = ctx;
        this.mList = mList;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.penyakit_detail_listview, parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        ViewDetailDataModel dm = mList.get(position);
        Picasso.with(ctx).load(base_url + mList.get(position).getIconPoli()).fit().centerCrop().into(holder.pictPoli);
        holder.namePoli.setText(dm.getNamaPoli());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class HolderData extends RecyclerView.ViewHolder{

        ImageView pictPoli;
        TextView namePoli;

        public HolderData(View v) {
            super(v);

            pictPoli = v.findViewById(R.id.iv_poli);
            namePoli = v.findViewById(R.id.tv_poli);
        }
    }

}
