package com.example.qtime.Model;

import java.util.List;

public class ResponseNewsModel {

    String kode, pesan;
    List<NewsModel> result;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<NewsModel> getResult() {
        return result;
    }

    public void setResult(List<NewsModel> result) {
        this.result = result;
    }

}
